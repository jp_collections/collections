const languages = new Set(["English", "French"]);

// https://mcc.gouv.qc.ca/index.php?id=5938#c33767
const ratings = new Set(["G", "13+", "16+", "18+", "E/C"]);

class Film {
  constructor(posterPath, name, year, director, language, lengthMin, rating = undefined) {
    const isValidYear = Number.isInteger(year) && year >= 0;
    const isValidLanguage = languages.has(language);
    const isValidLengthMin = Number.isInteger(lengthMin) && lengthMin >= 0;
    const isValidRating = rating === undefined || ratings.has(rating);

    this.posterPath = posterPath;
    this.name = name;
    this.year = isValidYear ? year : undefined;
    this.director = director;
    this.language = isValidLanguage ? language : undefined;
    this.lengthMin = isValidLengthMin ? lengthMin : undefined;
    this.rating = isValidRating ? rating : undefined;

    if (!isValidYear) {
      throw "The film '" + name + "' has an invalid year: " + year + ".";
    }

    if (!isValidLanguage) {
      throw "The film '" + name + "' has an invalid language: " + language + ".";
    }

    if (!isValidLengthMin) {
      throw "The film '" + name + "' has an invalid length (min): " + lengthMin + " .";
    }

    if (!isValidRating) {
      throw "The film '" + name + "' has an invalid rating: " + rating + ".";
    }
  }
}

// Creates list of films

var films = new Set();

function addFilmToList(posterPath, name, year, director, language, lengthMin, rating) {
  try {
    films.add(new Film(posterPath, name, year, director, language, lengthMin, rating));
  } catch (e) {
    console.error(e);
  }
}

// Special: Stanley Kubrick
{
  const director = "Stanley Kubrick";

  addFilmToList("images/kubrick_drstrangelove.jpg", "Dr. Strangelove", 1964, director, "English", 95, "G");
  addFilmToList("images/kubrick_2001.jpg", "2001: A space odyssey", 1968, director, "English", 150, "G");
  addFilmToList("images/kubrick_aclockworkorange.jpg", "A clockwork orange", 1971, director, "English", 140, "16+");
  addFilmToList("images/kubrick_theshining.jpg", "The shining", 1980, director, "English", 150, "13+");
}

// Year: 1980
{
  const year = 1980;

  addFilmToList("images/1980_thelastmetro.jpg", "The last metro", year, "François Truffaut", "French", 135);
  addFilmToList("images/1980_ordinarypeople.jpg", "Ordinary people", year, "Robert Redford", "English", 125);
  addFilmToList("images/1980_ragingbull.jpg", "Raging bull", year, "Martin Scorsese", "English", 130, "13+");
  addFilmToList("images/1980_theelephantman.jpg", "The elephant man", year, "David Lynch", "English", 125, "G");
}

// Special: Francis Ford Coppola
{
  const director = "Francis Ford Coppola";

  addFilmToList("images/coppola_thegodfather.jpg", "The godfather", 1972, director, "English", 175, "13+");
  addFilmToList("images/coppola_thegodfather_part2.jpg", "The godfather: Part II", 1974, director, "English", 205, "13+");
  addFilmToList("images/coppola_apocalypsenow.jpg", "Apocalypse now", 1979, director, "English", 150, "13+");
  addFilmToList("images/coppola_thegodfather_part3.jpg", "The godfather: Part III", 1990, director, "English", 165, "13+");
}

// Year: 1990
{
  const year = 1990;

  addFilmToList("images/1990_cyranodebergerac.jpg", "Cyrano de Bergerac", year, "Jean-Paul Rappeneau", "French", 140);
  addFilmToList("images/1990_danceswithwolves.jpg", "Dances with wolves", year, "Kevin Costner", "English", 185, "13+");
  addFilmToList("images/1990_goodfellas.jpg", "Goodfellas", year, "Martin Scorsese", "English", 150, "13+");
  addFilmToList("images/1990_prettywoman.jpg", "Pretty woman", year, "Garry Marshall", "English", 120, "G");
}

console.log("Unsorted films:");
console.log(films);

// Displays list of films

{
  let sortedFilms = [...films];

  sortedFilms.sort((a, b) => {
    let filmNameA = a.name;
    let filmNameB = b.name;
    return filmNameA < filmNameB ? -1 : filmNameA > filmNameB ? 1 : 0;
  });

  console.log("Sorted films:");
  console.log(sortedFilms);

  // Helper function to create info-data elements for each film
  function createInfoDataElement(elementClassName, infoClassName, infoTextContent, dataClassName, dataTextContent) {
    let info = document.createElement("span");
    info.className = infoClassName;
    info.textContent = infoTextContent;

    let data = document.createElement("span");
    data.className = dataClassName;
    data.textContent = dataTextContent;

    let element = document.createElement("div");
    element.className = elementClassName;
    element.append(info, data);

    return element;
  }

  let filmsList = document.getElementById("list_films");

  for (const film of sortedFilms) {
    // film_poster contains: film_poster_data

    let filmPosterData = document.createElement("img");
    filmPosterData.className = "film_poster_data";
    filmPosterData.src = film.posterPath;
    filmPosterData.alt = film.name;

    let filmPoster = document.createElement("div");
    filmPoster.className = "film_poster";
    filmPoster.appendChild(filmPosterData);

    // film_name, film_year, film_director, filmLanguage, filmDuration, filmRating

    let filmName = createInfoDataElement("film_name", "film_name_info", "Name:", "film_name_data", film.name);
    let filmYear = createInfoDataElement("film_year", "film_year_info", "Year:", "film_year_data", film.year);
    let filmDirector = createInfoDataElement("film_director", "film_director_info", "Director:", "film_director_data", film.director);
    let filmLanguage = createInfoDataElement("film_language", "film_language_info", "Language:", "film_language_data", film.language);
    let filmDuration = createInfoDataElement("film_duration", "film_duration_info", "Duration:", "film_duration_data", film.lengthMin + " min");
    let filmRating = createInfoDataElement("film_rating", "film_rating_info", "Rating:", "film_rating_data", film.rating);

    // film_item contains: film_poster, film_name, film_year, film_director, filmLanguage, filmDuration, filmRating

    let filmElement = document.createElement("div");
    filmElement.className = "film_item";
    filmElement.append(filmPoster, filmName, filmYear, filmDirector, filmLanguage, filmDuration, filmRating);

    // Appends to films list

    filmsList.append(filmElement);
  }
}
